package main

import (
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {
	config := initConfig()

	APIsecret := secret(32)
	log.Println(APIsecret)

	db := newDatabase(
		config.dbDriver,
		config.dbType,
		config.dbUser,
		config.dbPassword,
		config.dbHost,
		config.dbPort,
		config.dbName,
	)

	r := gin.Default()

	r.GET("/status", func(c *gin.Context) {
		current := c.Query("current")
		if current == "true" {
			id, status := db.current()
			c.JSON(http.StatusOK, gin.H{"id": id, "status": status})
			return
		}

		status, err := db.status()
		if err != nil {
			c.JSON(http.StatusNoContent, err)
			return
		}

		c.JSON(http.StatusOK, status)
	})

	r.GET("/update", func(c *gin.Context) {
		timestamp := c.Query("timestamp")
		if len(timestamp) < 1 {
			c.JSON(http.StatusNotFound, gin.H{"error": "missing timestamp"})
			return
		}

		timeProcessed, err := time.Parse(time.RFC822, timestamp)
		if err != nil {
			c.JSON(http.StatusUnprocessableEntity, err)
			return
		}

		updates, err := db.update(timeProcessed)
		if err != nil {
			c.JSON(http.StatusNoContent, err)
			return
		}

		c.JSON(http.StatusOK, updates)
	})

	r.POST("/update", func(c *gin.Context) {
		key := c.PostForm("api_key")
		if key != APIsecret {
			c.JSON(http.StatusUnauthorized, gin.H{"message": "invalid api_key"})
			return
		}

		statusID := c.PostForm("status_id")
		if len(statusID) < 1 {
			c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "missing status_id"})
			return
		}

		statusIDint, err := strconv.Atoi(statusID)
		if err != nil {
			c.JSON(http.StatusUnprocessableEntity, err)
			return
		}

		db.setUpdate(statusIDint)
		c.JSON(http.StatusOK, gin.H{"message": "success"})
	})

	r.Run()
}
