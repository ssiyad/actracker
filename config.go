package main

import "os"

type configuration struct {
	dbDriver   string
	dbType     string
	dbUser     string
	dbPassword string
	dbHost     string
	dbPort     string
	dbName     string
}

func initConfig() configuration {
	conf := configuration{}

	conf.dbDriver = os.Getenv("ACTRACKER_DB_DRIVER")
	conf.dbType = os.Getenv("ACTRACKER_DB_TYPE")
	conf.dbUser = os.Getenv("ACTRACKER_DB_USER")
	conf.dbPassword = os.Getenv("ACTRACKER_DB_PASSWORD")
	conf.dbHost = os.Getenv("ACTRACKER_DB_HOST")
	conf.dbPort = os.Getenv("ACTRACKER_DB_PORT")
	conf.dbName = os.Getenv("ACTRACKER_DB_NAME")

	return conf
}
